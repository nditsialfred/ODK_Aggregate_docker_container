-- Adapt this code to your conf

create database "odkdb";
create user "md" with password '!N63444764.';
grant all privileges on database "odkdb" to "md";
alter database "odkdb" owner to "md";

-- Connect to ODK DATABASE called odkdb
\c "odkdb";

create schema "odk_schema_name";
grant all privileges on schema "odk_schema_name" to "md";
alter schema "odk_schema_name" owner to "md";
      